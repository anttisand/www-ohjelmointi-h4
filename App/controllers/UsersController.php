<?php
namespace Wo\App\Controllers;

use Wo\Core\App;
use Wo\App\Models\User;
use Wo\Core\Validator;

class UsersController
{
	public function create()
	{
		return view("register");
	}

	public function save()
	{
		$req = App::get('request');

		$errors = (new Validator([
			'name' => 'required',
			'email' => 'required',
			'email' => 'validEmail',
			'password' => 'required'
		]))->validate();

		if(count($errors) > 0) {
			return view("register", compact("errors"));
		}

		User::create([
			'name' => $req->get('name'),
			'email' => $req->get('email'),
			'password' => password_hash($req->get('password'), PASSWORD_DEFAULT)
		]);

		return view("register", ["message" => "Account created?"]);
	}

	public function showLogin()
	{
		return view("login");
	}

	public function login()
	{
		$req = App::get('request');
		$user = User::findWhere('email', $req->get('email'));

		if($user && password_verify($req->get('password'), $user->password)) {
			$_SESSION['name'] = $user->name;
			$_SESSION['user_id'] = $user->id;

			header('Location: /todos');
		}

		return view("login", ["message" => "The email or password was invalid"]);
	}

	public function logout()
	{
		session_unset();
		session_destroy();

		return view("login", ["message" => "Session closed"]);
	}
}
