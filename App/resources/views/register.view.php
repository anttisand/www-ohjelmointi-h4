<?php require "_header.view.php"; ?>
	<div class="panel">
		<p class="panel-heading">Register account</p>
		<div class="panel-block">
			<?php if(isset($message)): ?>
				<div class="notification is-success">
					<?= $message ?>
				</div>
			<?php endif; ?>

			<?php if(isset($errors)): ?>
				<div class="notification is-warning">
					<ul>
				<?php foreach($errors as $error): ?>
					<li><?= $error; ?></li>
				<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>

			<form action="<?= url('/register') ?>" method="POST">

				<label class="label">Name</label>
				<p class="control">
					<input class="input" type="text" name="name" placeholder="Enter your name...">
				</p>

				<label class="label">Email</label>
				<p class="control">
					<input class="input" type="email" name="email" placeholder="Enter email...">
				</p>

				<label class="label">Password</label>
				<p class="control">
					<input class="input" type="password" name="password">
				</p>

				<p class="control">
					<button class="button is-primary">Register</button>
  				</p>
			</form>
		</div>
	</div>
<?php require "_footer.view.php"; ?>”