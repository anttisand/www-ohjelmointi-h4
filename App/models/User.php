<?php

namespace Wo\App\Models;

use Wo\Core\Database\Model;

class User extends Model
{
	protected static $tableName = 'users';
}