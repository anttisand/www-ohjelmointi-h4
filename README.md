# WWW-Ohjelmointi harjoitus 4

Harjoituksessa siirryttiin käyttämään nimiavaruuksia ja jaoteltiin kontrollerit luokiksi.

## Nimiavaruudet

Nimiavaruuksiin siirtyminen saattoi tuntua työläältä ennen kuin hahmotti, miten nimiavaruudet vastaavat hakemistorakennetta PSR-4:ssä ja miten luokkia tulisi _käyttää_ (use).

	use Wo\Core\Router;

Refaktorointi oli kuitenkin kannattava, sillä nyt voimme käyttää jatkossa PSR-4 autoloadia, eikä meidät tarvitse enää huolehtia luokkatiedostojen lataamisesta. Samalla saimme oman koodin siihen pisteeseen, että voimme hyödyntää muiden tekemiä paketteja vaivattomasti ilman pelkoa siitä, että ne aiheuttavat päälekkäisyyksiä.

Composer.json -tiedostossa määritetty päänimiavaruus vastaa nyt projektihakemistoa. Päänimiavaruutena on _Wo_ ja sen perässä olevat nimiavaruudet tulisi löytyä vastaavasta hakemistopolusta.

	use Wo\Core\Router;
	// Tulisi löytyä Core\Router.php -tiedostosta

PSR-4 autoload lataa luokkatiedostot nyt automaattisesti ilman tarvetta ajaa `composer dump-autoload` jokaisen muutoksen jälkeen. Meillä on kuitenkin muitakin tiedostoja, kuin luokkatiedostoja. Esimerkiksi `bootstrap.php`:ssa on jaettu omaan tiedostoonsa ohjelman valmisteluja, kuten asetusten lataaminen ja tietokantayhteyden määritteleminen. Tämä ei siis lataudu PSR-4:llä. Toisaalta meillä on myös `config.php` ja `routes.php`, jotka täytyy saada ladattua juuri tietyssä kohdassa suoritusta. Näille jälkimmäisille ei kannata tehdä mitään, sillä lataamalla ne suorituksen alussa, meidän tulisi tallentaa ne ensin muuttujaan, joka sitten annettaisiin hyödyntävälle koodille ja näin ne tulisivat globaaleiksi muuttujiksi.

`bootstrap.php` sen sijaan voidaan ladata automaattisesti, sillä se ei palauta mitään. Mutta koska se ei ole oma luokkansa, se ei lataudu PSR-4:llä. Se voidaan asettaa latautumaan lisäämällä `composer.json` -tiedostoon `files` -taulukko.

    "files": [
    	"core/bootstrap.php"
    ]

 Mutta koska tämä on eksplisiittinen määrite, jota ei voida päätellä ajon aikana, siitä täytyy muodostua avain-arvo -pari, joten se vaatii `composer dump-autoload` -komennon. Tällaisia aputiedostoja ei kuitenkaan tulisi olla kuin muutama, joten se on ihan hyväksyttävä kompromissi.

## Kontrollerit luokiksi

Nyt voimme määrittää vastattavat reitit kutsun tyypin mukaisesti. Kun aiemmin oltaisiin voitu pyytää tietoa tallentava reitti `GET` -kutsulla, nyt voimme määrittää sen saman `endpoint`:n vastaamaan eri tavalla `GET` ja `POST` -pyyntöihin. Samalla saimme koodin jaoteltua metodeihin. Noudattaessamme yleistä nimeämiskäyntöä on selvää, että jos pitää tehdä muutoksia siihen koodiin, joka tallentaa uuden resurssin järjestelmään, meidän tulee etsiä toteuttava koodi kyseisen resurssikontrollerin save -metodista.

### Metodien vastuut ja tilaviestien kuljettaminen

Metodit _save_, _update_ ja _delete_ tekevät vain oman vastuualueensa asioita, _i.e._ validoivat syötteen ja suorittavat tiedon varastointikerroksen kutsut. Ne eivät palauta tehtävälistausta tai näkymää, vaan ne palauttavat uudelleenohjauksen pakottavan _header_:n, joka ohjaa takaisin lukureittiin.

Koska tapahtuma aiheuttaa uudelleenohjauksen ja samalla uuden transaktion, meidän tulee jollain tavalla ylläpitää tilaa, jotta metodien tuottamat validointivirheviestit ja onnistumisviestit välittyisivät lukureitille ja näin käyttäjän nähtäväksi.

`index.php` -tiedostossa on nyt `session_start()` -funktiokutsu, joka aloittaa (tai jatkaa) istunnon. Voimme sen vuoksi tallentaa viestit istunnon muuttujiin (`$_SESSION['message'] = "Tiedot poistettu."`). Tällaiset tiedot ovat kuitenkin luonteeltaan sellaisia, joita tulisi säilyttää vain yhden transaktion ajan. Eihän käyttäjä halua nähdä viestiä _Tehtävä lisätty_ jokaisen pyynnön yhteydessä, vaan vain sillloin, kun edellinen transaktio on johtanut tehtävän lisäämiseen. Näistä käytetään termiä _flash message_. Yksinkertaisimmillaan sellainen voidaan toteuttaa `TasksController@index` -metodissa tarkistamalla onko meillä istunnossa viestiä ja sitten poistamalla viestin, jotta se ei välity enää myöhemmille transaktioille.

	$message = '';

	if(isset($_SESSION['message'])) {
		$message = $_SESSION['message'];
		unset($_SESSION['message']);
	}

Näin istunnossa mahdollisesti ollut tilaviesti on tallennettu paikallisesti `$message` -muuttujaan, mutta vain seuraavan transaktion ajaksi.

## Edelleen puutteita

Koska tietoa muokkaavien reittien tulee olla `POST` -pyyntöjä, voimme antaa instanssin tunnisteen lomakkeen piilokenttänä ja kiertää sen, että reitittimemme ei vielä ymmärrä `wildcard`:ja, eli siis että reitti vastaisi kaikkiin `/todos/{jokunumero}` -reitteihin kutsuen samaa metodia, mutta antaen parametrina yksilöivän tunnisteen. `GET` -pyynnöissä tämä ei vielä toimi, mutta voimme toki kiertää sen normaalisti `query string` -parametreilla (`GET /todos?id=1` reitittyisi samoin, kuin `GET /todos` ja annettu parametri voitaisiin hakea `App::get('request')->query->get('id')`). Toiminnallisuus olisi täsmälleen sama, mutta pyynnön polku ei noudattaisi RESTful -ajattelua. Tässä siis tehdään huomattava määrä työtä, joka ei muuta sovelluksen toimintaa, mutta saa vastattavat reitit näyttämään sovitun kaltaiselta.

Jotta voisimme reitittää RESTful -ajattelun mukaisesti yksittäisen resurssi-instanssin palauttavan reitin `GET /resurssi/tunniste`, meidän tulisi kyetä reitittämään sellaiset reitit, joiden tarkkaa sisältöä emme tiedä. Emmehän voi kirjoittaa erikseen kaikkia reittejä `/todos/1`, `/todos/2`, `/todos/3`, jne. Tarvitsemme jonkin toiminnallisuuden, jolla voimme sanoa, että kun saat pyynnön `/todos/[1-9]`, viet metodiin `TodosController@show` ja annat sille parametrina halutun resurssi-instanssin tunnisteen. Tämän voisi ratkaista vaikka säännöllisillä lausekkeilla (regular expressions).