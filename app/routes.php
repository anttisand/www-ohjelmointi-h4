<?php

$router->get('/', 'HomeController@index');
$router->get('/about', 'HomeController@about');

$router->get('/todos', 'TasksController@index');
$router->post('/todos', 'TasksController@save');
$router->post('/todos/merkkaa', 'TasksController@update');
$router->post('/todos/delete', 'TasksController@delete');

$router->get('/register', 'UsersController@create');
$router->post('/register', 'UsersController@save');
$router->get('/login', 'UsersController@showLogin');
$router->post('/login', 'UsersController@login');
$router->get('/logout', 'UsersController@logout');