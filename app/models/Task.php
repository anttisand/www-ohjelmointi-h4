<?php

namespace Wo\App\Models;

use Wo\Core\Database\Model;

class Task extends Model
{
	protected static $tableName = 'todo';

	public $description;
	public $completed;
	public $id;

	public function isCompleted()
	{
		return $this->completed;
	}

	public function complete()
	{
		$this->completed = true;
	}
}