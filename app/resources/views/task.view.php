<?php require "_header.view.php"; ?>

<?php if(isset($message) && strlen($message) > 0): ?>
<div class="notification is-primary">
	<?= $message; ?>
</div>
<?php endif; ?>

<nav class="panel">
	<p class="panel-heading">
		Tehtävälista
	</p>

	<?php foreach ($tasks as $task):?>
		<label class="panel-block">
			<input
				type="checkbox"
				disabled="disabled"
				<?php if($task->isCompleted()) { echo ' checked="checked"'; } ?>
			>
				<?= htmlspecialchars($task->description); ?>
				<?php if(!$task->isCompleted()): ?>
					- [
					<form action="<?= url('/todos/merkkaa') ?>" method="POST">
						<input type="hidden" name="id" value="<?= $task->id; ?>">
						<button type="submit" class="button is-link">Merkkaa valmiiksi</button>
					</form>
					]
				<?php endif; ?>
				- [
				<form action="<?= url('/todos/delete') ?>" method="POST">
						<input type="hidden" name="id" value="<?= $task->id; ?>">
						<button type="submit" class="button is-link">Poista</button>
					</form>
				] - <span class="tag is-primary"><?= htmlspecialchars($task->duedate); ?></span>
		</label>
	<?php endforeach;?>
</nav>

<h1 class="title">Lisää uusi tehtävä</h1>

<div class="notification">
	<form action="<?= url('/todos') ?>" method="POST">

		<input type="hidden" name="token" value="<?= $token ?>">

		<label class="label" for="description">Anna kuvaus</label>
		<p class="control">
			<input class="input" type="text" id="description" name="description">
		</p>

		<label class="label" for="duedate">Anna määräaika</label>
		<p class="control">
			<input class="input" type="text" id="duedate" name="duedate">
		</p>

		<p class="control">
			<input class="button is-primary" type="submit" value="Lisää uusi tehtävä">
		</p>
	</form>
</div>
<?php require "_footer.view.php"; ?>