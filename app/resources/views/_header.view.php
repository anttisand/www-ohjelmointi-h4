<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>H5</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.0/css/bulma.min.css"/>
</head>
<body>

<section class="hero is-medium is-primary is-bold">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">WWW-ohjelmointi</title>
      <h2 class="subtitle">Todo -app</h2>
    </div>
  </div>
</section>

<div class="tabs is-centered">
  <ul>
    <li><a href="/">Etusivu</a></li>
    <li><a href="<?= url('/about') ?>">Tietoja sovelluksesta</a></li>
    <li><a href="<?= url('/todos') ?>">Taskit</a></li>
    <?php if(isset($_SESSION['user_id'])): ?>
      <li><a href="<?= url('/logout') ?>">Kirjaudu ulos</a></li>
    <?php else: ?>
      <li><a href="<?= url('/login') ?>">Kirjaudu sisään</a></li>
      <li><a href="<?= url('/register') ?>">Rekisteröidy</a></li>
    <?php endif; ?>
  </ul>
</div>

    <section class="section">
        <div class="container">
