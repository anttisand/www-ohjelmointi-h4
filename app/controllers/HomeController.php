<?php

namespace Wo\App\Controllers;

class HomeController
{
	public function index()
	{
		$message = "Tämä on etusivu";

		return view('index', compact('message'));

	}

	public function about()
	{
		$message = "Tämä on about-sivu";

		return view('index', compact('message'));
	}
}
