<?php

namespace Wo\App\Controllers;

use Wo\App\Models\Task;
use Wo\Core\App;
use Wo\Core\Gate;

class TasksController
{
	public function __construct()
	{
		// Täällä voitaisiin määrittää, että kaikki kontrollerin metodit ovat suojattuja

		// if(!Gate::can('see-tasks')) {
		// 	return header('Location: /login');
		// }
	}

	public function index()
	{
		// Tai voidaan määrittää jokaisen toiminnon kohdalla erikseen
		if(!Gate::can('see-tasks')) {
			return header('Location: /login');
		}

		$tasks = Task::all();

		$message = '';

		if(isset($_SESSION['message'])) {
			$message = $_SESSION['message'];
			unset($_SESSION['message']);
		}

		$_SESSION['token'] = '';
		$token= md5(mt_rand(1,1000000) . 'fuubar');
		$_SESSION['token'] = $token;

		return view('task', compact('message', 'tasks', 'token'));
	}

	public function save()
	{
		// Voitaisiin määrittää jokaiselle muokkaukselle oma oikeutensa
		// if(!Gate::can('create-tasks')) {
		// 	return header('Location: /login');
		// }

		$request = App::get('request')->request;

		//Tarkistetaan CSRF
		if(!isset($_SESSION['token'])
			|| $request->get('token') !== $_SESSION['token']) {
			throw new \Exception('CSRF TOKEN MISMATCH EXCPETION');
		}

		if(
			strlen($request->get('description')) < 1
			|| strlen($request->get('duedate')) < 1
			|| !preg_match('/^\d{1,2}\.\d{1,2}\.\d{4}$/', $request->get('duedate'))
		)
		{
			$_SESSION['message'] = 'Täytä kaikki lomakkeen tiedot! Päivämäärä muodossa dd.mm.yyyy';

			return header('Location: /todos');
		}

		Task::create([
			'description' => $request->get('description'),
			'duedate' => $request->get('duedate')
		]);

		$_SESSION['message'] = 'Tehtävä lisätty';

		header('Location: /todos');
	}

	public function update()
	{
		$request = App::get('request')->request;

		if(!$request->has('id')) {
			$_SESSION['message'] = 'Virheellinen kutsu, id puuttui';

			return header('Location: /todos');
		}

		Task::update($request->get('id'), [
			'completed' => true
		]);

		$_SESSION['message'] = 'Tehtävä päivitetty';

		header('Location: /todos');
	}

	public function delete()
	{
		$request = App::get('request')->request;

		if(!$request->has('id')) {
			$_SESSION['message'] = 'Virheellinen kutsu, id puuttui';

			return header('Location: /todos');
		}

		Task::delete($request->get('id'));

		$_SESSION['message'] = 'Tehtävä poistettu';

		header('Location: /todos');
	}
}
