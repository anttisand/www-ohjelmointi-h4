<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Wo\Core\App;
use Wo\Core\Database\QueryBuilder;
use Wo\Core\Database\Connection;
use Symfony\Component\HttpFoundation\Request;

App::bind('config', require 'core/config.php');

App::bind('database', new QueryBuilder(
	Connection::make(App::get('config')['database'])
));

App::bind('request', Request::createFromGlobals());