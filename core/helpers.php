<?php

function view($template, $data = [])
{
	extract($data);
	return require "app/resources/views/{$template}.view.php";
}

function url($route)
{
	return \Wo\Core\App::get('config')['alkuosa'] . $route;
}