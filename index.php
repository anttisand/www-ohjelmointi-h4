<?php

session_start();

require 'vendor/autoload.php';

use Wo\Core\Router;
use Wo\Core\App;

// if(App::get('request')->getMethod() == 'POST') {
// 	if(!App::get('request')->get('token') !== $_SESSION['token']) {
// 		throw new \Exception('CSRF TOKEN MISMATCH EXCPETION');
// 	}
// }

// $token= md5(mt_rand(1,1000000) . 'fuubar');
// App::bind('token', $token);
// $_SESSION['token'] = $token;

Router::define('app/routes.php')
	->fire(
		App::get('request')->getPathInfo(),
		App::get('request')->getMethod()
	);